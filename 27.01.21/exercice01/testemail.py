import unittest

import email



class TestEmail(unittest.TestCase):

    def test_at(self):

        self.assertFalse(email.check("bob.lennon.gmail.com"))

        self.assertTrue(email.check("bob.lennon@gmail.com"))

    def test_uppercase(self):

         self.assertFalse(email.check("BOB.lennon@gmail.com"))

    def test_punctuation(self):

          self.assertFalse(email.check("bob;lennon@gmail.com."))

    def test_accent(self):

         self.assertFalse(email.check("bob.lénnon@gmâil.com"))

    def test_extension(self):

         self.assertFalse(email.check("bob.lennon@gmail.cam"))

    def test_size(self):

         self.assertFalse(email.check("boooooooooooooooooooooooooooooob.lennoooooooooooooooooooooooon@gmail.com"))



if __name__ == '__main__':

    unittest.main()
