#Yann Lebouc
#Switch It Dev B2
#Exercice 02

#On demande les nombres qui vont composer l'opération
nombre_1 = int(input("Entrez votre premier numéro: "))
#On demande quel type d'oppération en expliquant les choix possibles
operation = str(input("""
	Choissisez le type d'opération :
	+ pour une addition
	- pour une soustraction
	* pour une multiplication
	/ pour une division

	Votre choix : """))
#On demande les nombres qui vont composer l'opération
nombre_2 = int(input("Entrez votre deuxième numéro: "))
#declarer la variable resultat pour pouvoir la récupérer plus tard
resultat = 0
#Si opération à une valeur de "+"
if operation == "+" :
	 resultat = (nombre_1 + nombre_2)
	 print(resultat)
#Si opération à une valeur de "-"
elif operation == "-" :
	 resultat = (nombre_1 - nombre_2)
	 print(resultat)
#Si opération à une valeur de "*"
elif operation == "*" :
	 resultat = (nombre_1 * nombre_2)
	 print(resultat)
#Si opération à une valeur de "/"
elif operation == "/" :
	 resultat = (nombre_1 / nombre_2)
	 print(resultat)
#Sinon on affiche une erreur
else :
	 print("erreur");

#Si le résultat à une valeur, on enregistre la valeur dans un fichier
if resultat :
#Il faut transformr resultat en str pour le fichier texte
	resultat_srt = str(resultat)
	#On demande le nom du fichier a l'utilisateur
	nomfichier = str(input("Entrez le nom de votre fichier pour sauvegarder votre résultat : "))
	#Ouvre un dossier et y note le resultat puis le ferme
	fichier = open(nomfichier+".txt", "w")
	fichier.write(resultat_srt)
	fichier.close


