#Yann Lebouc
#Switch It Dev B2
#Exercice 01

#Invite l'utilisateur à entrer sa phrase. Elle se stock dans la varible phrase.
phrase = str(input("Entrez une phrase : "))
#On découpe la phrase avec .split()
phrase_decoupe = phrase.split()
#On affiche la phrase sans découpage
print(phrase)
#Puis avec découpage
print(phrase_decoupe)

#Et si on mettais tout en capital ?
print("Je ne vois pas très bien...")
print(phrase.upper())
#Et si on mellangeait des lettres et des chiffres ?
print("Des lettres a et des chiffres 4 ?")
print(phrase.replace("a","4"))
