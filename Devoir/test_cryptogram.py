import unittest
import card

class TestFonc(unittest.TestCase):
    #Test if the code is accepted only with three characters
    def test_cvc_size(self):
        self.assertTrue(card.cvc_card('205'))
        self.assertTrue(card.cvc_card('854'))
        self.assertFalse(card.cvc_card('1'))
        self.assertFalse(card.cvc_card('15'))
        self.assertFalse(card.cvc_card('1552'))

    #Test if the code is accepted only with positive characters
    def test_cvc_value(self):
        self.assertTrue(card.cvc_card('543'))
        self.assertRaises(ValueError, card.cvc_card('-452'))
