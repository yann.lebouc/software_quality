import unittest
import card


class TestFonc(unittest.TestCase):
    #check if the size number is equal to 16
    def test_size_16(self):
        self.assertTrue(card.numero_card('1492 1492 1492 1492'))
        self.assertFalse(card.numero_card('1492 1492 1492 '))

    # double each number in an even case. Start from the last number in the code number
    #if the code is hither than 9, we calculate the sum of both number who compose it. (ex : 12 => 1+2 => 3)
    def test_first_step(self):
        # test with the last number who is odd and the number before the last who is even
        self.assertEqual(card.multiplicate_code(1492149214921481),  2492249224922471)

        # test with the last number who is even and the number before the last who is odd
        self.assertEqual(card.multiplicate_code(1492149214921494),  2492249224922494)

        # test with the last number who is even and the number before the last who is even
        self.assertEqual(card.multiplicate_code(1492149214921486),  2492249224922476)

        # test with the last number who is odd and the number before the last who is odd
        self.assertEqual(card.multiplicate_code(1492149214921473),  2492249224922453)

    #Do the sum of every number in the code number
    def test_second_step(self):
        self.assertEqual(card.add_code(2492249224922471), 65)
        self.assertEqual(card.add_code(2492249224922494), 70)
        self.assertEqual(card.add_code(2492249224922476), 70)
        self.assertEqual(card.add_code(2492249224922453), 65)
        self.assertEqual(card.add_code(2641689428644081), 73)
        self.assertRaises(ValueError, card.add_code(-1341189418641071))

    def test_third_step(self):
        self.assertTrue(card.check_result(10))
        self.assertTrue(card.check_result(20))
        self.assertTrue(card.check_result(100))
        self.assertTrue(card.check_result(110))
        # 140 is the last valid number
        self.assertTrue(card.check_result(140))
        self.assertFalse(card.check_result(101))
        self.assertFalse(card.check_result(150))
        self.assertFalse(card.check_result(25))
        self.assertFalse(card.check_result(44))
        self.assertFalse(card.check_result(33))
        self.assertFalse(card.check_result(76))
        self.assertFalse(card.check_result(67))
        self.assertFalse(card.check_result(89))
        self.assertFalse(card.check_result(98))
        self.assertFalse(card.check_result(52))

if __name__ == '__main__':
    unittest.main()